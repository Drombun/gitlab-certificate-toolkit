# Installing a new certificate

## Manually install a new certificate in /etc/ssl

Check your NGINX configuration. If there are any problems, consider fixing them first.

```
sudo nginx -t
```

First make a backup of `/etc/ssl`.

```
export BACKUP="/root/etc-ssl-$(date)"
sudo mkdir -p "$BACKUP"
sudo chmod 700 "$BACKUP"
sudo cp -Pr /etc/ssl/ "$BACKUP/"
```

Next, copy the new certificates from `/root/` to `/etc/ssl`

```
export DOMAIN=www.example.com

sudo cp -v /root/$DOMAIN.crt /root/$DOMAIN.key /etc/ssl/
```

Now test your NGINX configuration.

```
sudo nginx -t
```

If there is an error, go through the configuration files in
`/etc/nginx/sites-enabled`.

```
sudo grep -r /etc/ssl/$DOMAIN /etc/nginx/sites-enabled/
```

If everything is OK, restart NGINX with:

```
sudo kill -HUP $(pgrep -f 'nginx.*master')
```

Finally check if the new certificate is being served using your web browser.
The start and end date of the certificate are a good way to check if it is the
right one.

## Troubleshooting example

```
root@blue-moon:~# sudo nginx -t
nginx: [emerg] SSL_CTX_use_PrivateKey_file("/etc/ssl/www.gitlab.com.key") failed (SSL: error:0B080074:x509 certificate routines:X509_check_private_key:key values mismatch)
nginx: configuration file /etc/nginx/nginx.conf test failed
root@blue-moon:~# grep -r /etc/ssl/www.gitlab.com /etc/nginx/sites-enabled/
/etc/nginx/sites-enabled/www.gitlab.com:  ssl_certificate /etc/ssl/www.gitlab.com.bundle;
/etc/nginx/sites-enabled/www.gitlab.com:  ssl_certificate_key /etc/ssl/www.gitlab.com.key;
```

Here the configuration file expects the certificate to be in
`www.gitlab.com.bundle` while it is really in `www.gitlab.com.crt`. We edit
`/etc/nginx/sites-enabled/www.gitlab.com` to use the `.crt` and try again.

```
root@blue-moon:~# sudo nginx -t
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```
